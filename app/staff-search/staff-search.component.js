'use strict';

// Register `staffSearch` component, along with its associated controller and template
angular.
  module('staffSearch').
  component('staffSearch', {
    templateUrl: 'app/staff-search/staff-search.template.html',
    controller: ['$http', function StaffSearchController($http) {
      var self = this;

      $http.get('app/data/staff.json').then(function(response) {
        self.staff = response.data;
      });
    }]
  });
