'use strict';

// Register `staffSearch` component, along with its associated controller and template
angular.
  module('staffSearch').
  filter('searchFilter', function() {
    return function(input, params) {
      if (params) {
        var output = [];
        var criteria = params.split(/(\s+)/);
        var name = '';
        var role = '';
        var office = '';
        for (var i = 0; i < criteria.length; i++) {
          var conditions = criteria[i].split(':');
          if (conditions.length == 2) {
            if (conditions[0].toLowerCase() == 'name') {
              name = conditions[1];
            } else if (conditions[0].toLowerCase() == 'role') {
              role = conditions[1];
            } else if (conditions[0].toLowerCase() == 'office') {
              office = conditions[1];
            }
          }
        }
        angular.forEach(input, function(item) {
          var matchName = name == '' || item.name.toLowerCase().indexOf(name.toLowerCase()) >= 0;
          var matchRole = role == '' || item.role.toLowerCase().indexOf(role.toLowerCase()) >= 0;
          var matchOffice = office == '' || item.office.toLowerCase().indexOf(office.toLowerCase()) >= 0;
          if (matchName && matchRole && matchOffice) {
            output.push(item);
          }

        })
        return output;
      } else {
        return input;
      }
    };
  });
